FROM debian:latest

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        ca-certificates \
        git \
        jq \
        moreutils \
        patch \
        xmlstarlet \
    && rm -rf /var/lib/apt/lists/* \
    && ln -s /usr/bin/xmlstarlet /usr/local/bin/xml

COPY git-mkver yq /usr/local/bin/
