#!/usr/bin/env sh
GITHUB_REPOSITORY_URL="https://github.com/idc101/git-mkver"

LATEST_RELEASE_TAG="$(./get-latest-release-tag.sh "${GITHUB_REPOSITORY_URL}")"
LATEST_VERSION="$(echo "${LATEST_RELEASE_TAG}" | awk -F'^v?' '{print $2}')"

echo "git-mkver: LATEST_VERSION=${LATEST_VERSION}"
URL="https://github.com/idc101/git-mkver/releases/download/v${LATEST_VERSION}/git-mkver-linux-x86_64-${LATEST_VERSION}.tar.gz"
echo "git-mkver: URL=${URL}"
curl -sL "${URL}" | tar zxv --no-same-owner -C "$(pwd)"

test -f "git-mkver"
